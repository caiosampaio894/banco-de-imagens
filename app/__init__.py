from posix import listdir
import re
from typing import final
from flask import Flask, json, request, safe_join, jsonify, send_from_directory, abort
import zipfile
import os.path
import os
import ipdb


from werkzeug.exceptions import NotFound
from werkzeug.utils import secure_filename

app= Flask(__name__)

app.config["MAX_CONTENT_LENGTH"] = 1 * 1024 * 1024


from environs import Env

env = Env()
env.read_env()

BANK = env('FLASK_MY_PATH')
BANKPNG = './imagens/png'
BANKJPG = './imagens/jpg'
BANKGIF = './imagens/gif'



ALLOWED_IMAGES = ['png', 'jpg', 'gif']
import os

if not os.path.exists(BANK):
    os.makedirs(BANK)

if not os.path.exists(BANKPNG):
    os.makedirs(BANKPNG)

if not os.path.exists(BANKJPG):
    os.makedirs(BANKJPG)

if not os.path.exists(BANKGIF):
    os.makedirs(BANKGIF)
  


def allow_images(filename):
    # return images.allow_images(images.filename)
    if not '.' in filename:
        return False
        
    final_name = filename.rsplit('.', 1)[1]
    
    if final_name in ALLOWED_IMAGES:
        return final_name
    else: 
        return False   

@app.route('/upload', methods=['POST'])
def second_function():
    # return images.upload_pictures()
    arquivo = request.files['file']
    novo_arquivo = secure_filename(arquivo.filename)
    final_name = (arquivo.filename).rsplit('.', 1)[1]


    print(novo_arquivo, '**')
    print(f'{BANK}/{final_name}/{novo_arquivo}')
    
    if not allow_images(arquivo.filename) in ALLOWED_IMAGES:
        return {'message': 'Type of file is not allowed'}

    file_name_exist = os.listdir(f'{BANK}/{final_name}')
    print(file_name_exist)
    print(novo_arquivo)


    if novo_arquivo in file_name_exist:
        return {'message': f'{novo_arquivo} já existe'}, 409

    file_path = safe_join(f'{BANK}/{final_name}', arquivo.filename)

    arquivo.save(file_path)

    # if allow_images(arquivo.filename) == 'png':
    #     novo_arquivo = safe_join(BANKPNG, arquivo.filename)

    # if allow_images(arquivo.filename) == 'gif':
    #     novo_arquivo = safe_join(BANKGIF, arquivo.filename)

    # if allow_images(arquivo.filename) == 'jpg':
    #     novo_arquivo = safe_join(BANKJPG, arquivo.filename)    

      
    # print(final_name)
    # print(novo_arquivo)
    # print(arquivo) 
    
    # with open(novo_arquivo, 'wb') as file:
    #     file.write(request.data)

    return  {'message': f'{novo_arquivo}, created'}, 201

       
      

    
@app.route('/files', methods=['GET'])
def third_function():
    # return images.get_files()
    list_files = list()
    for _, _, files in os.walk(BANK):
        list_files += [file for file in files]

    print(list_files)
    return jsonify(list_files), 200


@app.route('/files/<type_of_files>', methods=['GET'])
def fourth_function(type_of_files):
    # return images.get_type_of_files(images.type_of_files)
    new_list = list()
    for _, _, files in os.walk(f'{BANK}/{type_of_files}'):
        new_list += [file for file in files]

    return jsonify(new_list), 200

@app.route('/download/<file_name>', methods=['GET'])
def fifth_function(file_name):
    final_name = file_name.rsplit('.', 1)[1]
    try:
        os.listdir(f'{BANK}/{final_name}')
        return send_from_directory(
            directory=f'.{BANK}/{final_name}', path=file_name, as_attachment=True
            ), 200
    except NotFound:
        return {'message': 'File not found'}, 404  


@app.route('/download-zip', methods=['GET'])
def sixth_function():
    # return images.download_zip()

    file_type = request.args.get('file_type')
    compression_rate = request.args.get('compression_rate')

    if not os.listdir(f'{BANK}/{file_type}'):
        return {'message': 'Directory is empty'}, 404

    else:
        zip_file = f'zip -r /tmp/{file_type}.zip {BANK}/{file_type} {compression_rate}'
        os.system(zip_file)

        return send_from_directory(
            directory='/tmp', path=f'{file_type}.zip', as_attachment=True
        ), 200
       

    # except NotFound:
    #     return {'message': 'File not found'}, 404

    